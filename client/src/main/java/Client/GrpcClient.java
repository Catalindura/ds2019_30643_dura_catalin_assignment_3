package Client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.baeldung.grpc.IntakeResponse;
import org.baeldung.grpc.MedicalPlanGrpc;
import org.baeldung.grpc.MedicalPlanRequest;
import org.baeldung.grpc.MedicalPlanResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GrpcClient {

    public List<MedicalPlanResponse> medicalPlanResponses;

    public void getIntakes() throws  InterruptedException{
        medicalPlanResponses=new ArrayList<MedicalPlanResponse>();
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();
        MedicalPlanGrpc.MedicalPlanBlockingStub stub
                = MedicalPlanGrpc.newBlockingStub(channel);

        MedicalPlanRequest medicalPlanRequest= MedicalPlanRequest.newBuilder()
                .setLastName("Catalin")
                .setFirstName("Dura")
                .build();

        Iterator<MedicalPlanResponse> medicalPlanResponseIterator=stub.getIntakes(medicalPlanRequest);
        while(medicalPlanResponseIterator.hasNext()) {
            medicalPlanResponses.add(medicalPlanResponseIterator.next());
        }
        channel.shutdown().awaitTermination(1, TimeUnit.DAYS);
    }

    public void sendIntake(MedicalPlanResponse medicalPlanResponse) throws  InterruptedException{

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();
        MedicalPlanGrpc.MedicalPlanBlockingStub stub
                = MedicalPlanGrpc.newBlockingStub(channel);
        IntakeResponse intakeResponse=stub.sendIntake(medicalPlanResponse);
        System.out.println(intakeResponse);
        channel.shutdown().awaitTermination(1, TimeUnit.DAYS);
    }

    public static void main(String[] args) throws InterruptedException {
        GrpcClient grpcClient=new GrpcClient();
        grpcClient.getIntakes();
        for(MedicalPlanResponse aux:grpcClient.medicalPlanResponses){
            System.out.println(aux);
        }
    }
}
