package View;

import Client.GrpcClient;
import org.baeldung.grpc.IntakeResponseOrBuilder;
import org.baeldung.grpc.MedicalPlanResponse;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class IntakeTable extends JFrame{
    GrpcClient grpcClient=new GrpcClient();
    int time=0;
    private String init="false";
    List<MedicalPlanResponse> intakes=new ArrayList<MedicalPlanResponse>();
    private JTable tabel=new JTable();
    private JScrollPane scroll;
    private JTextField timer=new JTextField(4);
    private JButton take=new JButton("take");
    private JLabel label=new JLabel("Timer");
    private JButton startTimer=new JButton("Start timer");

    public IntakeTable() throws  InterruptedException{
        JPanel p1=new JPanel();
        JPanel p2=new JPanel();
        JPanel p3=new JPanel();
        p1.add(label);
        p1.add(timer);
        p1.add(startTimer);
        scroll=new JScrollPane(tabel);
        p2.add(scroll);
        p3.add(p1);
        p3.add(p2);
        this.setTitle("Students table");
        this.add(p3);
        this.setSize(500,400);
        this.setVisible(true);
    }
    public void setTimer(String value){
        timer.setText(value);
    }
    public void downloadIntake()throws InterruptedException{
        grpcClient.getIntakes();
        intakes=grpcClient.medicalPlanResponses;

    }

    public void createJTable() throws InterruptedException{
        int col = 7;
        String[] coloane = new String[col];
        String[][] matrice = new String[intakes.size()][col];
        coloane[0]="Id";
        coloane[1] = "Name Patient";
        coloane[2] = "Start time";
        coloane[3]="End time";
        coloane[4]="Name Medication";
        coloane[5]="Taken";
        coloane[6]="Take";
        int j = 0;
        for (int i = 0; i < intakes.size(); i++) {
            System.out.println(intakes.get(i).getTaken());
            if(Integer.parseInt(intakes.get(i).getEndTime())<time){
                try {
                    grpcClient.sendIntake(MedicalPlanResponse.newBuilder()
                            .setId(intakes.get((i)).getId())
                            .setNamePatient(intakes.get(i).getNamePatient())
                            .setNameMedication(intakes.get(i).getNameMedication())
                            .setStartTime(intakes.get(i).getStartTime())
                            .setEndTime(intakes.get(i).getEndTime())
                            .setTaken("not taken")
                            .build());
                    System.out.println(i);
                    intakes.remove(i);
                }
                catch(InterruptedException ex){
                    System.out.println(ex.getStackTrace());
                }
            }
            if(intakes.size()>0)
            if(intakes.get(i).getTaken().equals("---"))
            if (Integer.parseInt(intakes.get(i).getStartTime()) <= time && Integer.parseInt(intakes.get(i).getEndTime()) >= time) {
                matrice[j][0] = intakes.get(i).getId();
                matrice[j][1] = intakes.get(i).getNamePatient();
                matrice[j][2] = intakes.get(i).getStartTime();
                matrice[j][3] = intakes.get(i).getEndTime();
                matrice[j][4] = intakes.get(i).getNameMedication();
                matrice[j][5] = intakes.get(i).getTaken();
                matrice[j][6] = "take";
                j++;
            }
        }

        DefaultTableModel model = new DefaultTableModel(matrice, coloane);
        tabel.setModel(model);
        Action delete = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                JTable table = (JTable)e.getSource();
                int row=((JTable)e.getSource()).getSelectedRow();
                String id=table.getValueAt(row,0).toString();
                try {
                    MedicalPlanResponse sent= MedicalPlanResponse.newBuilder()
                            .setId(table.getValueAt(row, 0).toString())
                            .setNamePatient(table.getValueAt(row, 1).toString())
                            .setNameMedication(table.getValueAt(row, 4).toString())
                            .setStartTime(table.getValueAt(row, 2).toString())
                            .setEndTime(table.getValueAt(row, 3).toString())
                            .setTaken("taken")
                            .build();
                    grpcClient.sendIntake(sent);
                    intakes.remove(row);
                }catch (InterruptedException err){
                    err.printStackTrace();
                }
                int modelRow = Integer.valueOf( e.getActionCommand() );
                ((DefaultTableModel)table.getModel()).removeRow(modelRow);
            }
        };
        ButtonColumn buttonColumn = new ButtonColumn(tabel, delete, 6);
        buttonColumn.setMnemonic(KeyEvent.VK_D);
    }
    public void run(){
        int count=0;
        while (count<24) {
            try {
                Thread.sleep(2000);
                count++;
                time=count;
                setTimer(count+"");
                createJTable();
                if(count==3 && init.equals("false")){
                    downloadIntake();
                    init="true";
                    count=0;
                }
            } catch (InterruptedException e) {
                System.out.println(e.getStackTrace());
            }
        }
    }

    public static void main(String[] args) throws  InterruptedException{
        IntakeTable intakeTable=new IntakeTable();
        intakeTable.run();
    }
}