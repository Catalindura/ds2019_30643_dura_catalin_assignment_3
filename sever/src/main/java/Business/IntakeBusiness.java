package Business;

import Entity.Intake;
import RepositoryImpl.IntakeImpl;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class IntakeBusiness {
    IntakeImpl intakeImpl = new IntakeImpl();

    public List<Intake> getAllIntakes() {
        return intakeImpl.getAllIntakes();
    }

    public Intake getIntakeById(int id){
        return intakeImpl.getIntakeById(id);
    }

    public void updateIntake(Intake intake){
        intakeImpl.updateIntake(intake);
    }

    public void updateAll(){
        List<Intake> intakes=intakeImpl.getAllIntakes();
        for(Intake intake:intakes){
            intake.setTaken("---");
            intakeImpl.updateIntake(intake);
        }
    }
}