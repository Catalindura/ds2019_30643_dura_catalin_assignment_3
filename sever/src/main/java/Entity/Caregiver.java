package Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="caregiver")
public class Caregiver {
    @Id
    @Column(name="idcaregiver",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCaregiver;

    @OneToOne
    @JoinColumn(name="iduser")
    private User user;

    @OneToOne
    @JoinColumn(name="iddoctor")
    private Doctor doctor;

    @Column(name="name")
    private String name;

    @Column(name="address")
    private String address;

    @Column(name="gender")
    private String gender;

    @Column(name="birthdate")
    private String birthDate;


    @OneToMany(mappedBy = "caregiver",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Patient> patients;

    public Caregiver(){}

    public Caregiver(Integer idCaregiver, User user, Doctor doctor, String name, String address, String gender, String birthDate) {
        this.idCaregiver=idCaregiver;
        this.user = user;
        this.doctor = doctor;
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Integer getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(Integer idCaregiver) {
        this.idCaregiver = idCaregiver;
    }

    public User getUser() {
        return user;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
