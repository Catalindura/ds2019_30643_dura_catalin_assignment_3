package Entity;

import javax.persistence.*;

@Entity
@Table(name="intake")
public class Intake {
    @Id
    @Column(name="idintake",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idIntake;


    @ManyToOne
    @JoinColumn(name="idpatient")
    private Patient patient;


    @ManyToOne
    @JoinColumn(name="idmedication")
    private Medication medication;

    @Column(name="starttime")
    private String startTime;

    @Column(name="endtime")
    private String endTime;

    @Column(name="taken")
    private String taken;

    public Intake(){}

    public Intake(Integer idIntake,Patient patient, Medication medication, String startTime, String endTime,String taken) {
        this.idIntake=idIntake;
        this.patient = patient;
        this.medication = medication;
        this.startTime = startTime;
        this.endTime = endTime;
        this.taken=taken;
    }

    public Integer getIdIntake() {
        return idIntake;
    }

    public void setIdIntake(Integer idIntake) {
        this.idIntake = idIntake;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }
}
