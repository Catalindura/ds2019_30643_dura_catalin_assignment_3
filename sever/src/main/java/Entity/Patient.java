package Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="patient")
public class Patient {
    @Id
    @Column(name="idpatient",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPatient;

    @OneToOne
    @JoinColumn(name="iduser")
    private User user;

    @OneToOne
    @JoinColumn(name="idcaregiver")
    private Caregiver caregiver;

    @Column(name="name")
    private String name;

    @Column(name="address")
    private String address;

    @Column(name="gender")
    private String gender;

    @Column(name="birthdate")
    private String birthDate;

    @OneToMany(mappedBy = "patient",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Intake> intakes;

    public Patient(){}

    public Patient(Integer idPatient,User user,Caregiver caregiver, String name, String address, String gender, String birthDate) {
        this.idPatient = idPatient;
        this.user = user;
        this.caregiver = caregiver;
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public List<Intake> getIntakes() {
        return intakes;
    }

    public void setIntakes(List<Intake> intakes) {
        this.intakes = intakes;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "idPatient=" + idPatient +
                ", user=" + user +
                ", caregiver=" + caregiver +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", intakes=" + intakes +
                '}';
    }
}
