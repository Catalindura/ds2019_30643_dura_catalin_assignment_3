package Repository;

import Entity.Intake;

import java.util.List;

public interface IntakeRepository {
    public  List<Intake> getAllIntakes();
    public Intake getIntakeById(int id);
    public void updateIntake(Intake intake);
}
