package RepositoryImpl;

import Entity.Intake;
import Repository.IntakeRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import Hybernate.Hybernate;
import org.hibernate.criterion.Restrictions;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class IntakeImpl implements IntakeRepository {

    private SessionFactory sessionFactory= Hybernate.getSessionFactory();


    @Override
    public List<Intake> getAllIntakes() {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Intake.class);
        List <Intake> results = criteria.list();
        session.close();
        return results;
    }

    @Override
    public Intake getIntakeById(int id){
        Session session=sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Intake.class);
        criteria.add(Restrictions.like("idIntake",id));
        Intake result=(Intake)criteria.list().get(0);
        session.close();
        return result;
    }

    @Override
    public void updateIntake(Intake intake){
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.merge(intake);
        session.getTransaction().commit();
        session.close();
    }
}
