package server;

import Business.IntakeBusiness;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GrpcServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        IntakeBusiness intakeBusiness=new IntakeBusiness();
        intakeBusiness.updateAll();
        Server server = ServerBuilder.forPort(8080)
          .addService(new MedicalPlanServiceImpl()).build();

        System.out.println("Starting server...");
        server.start();
        System.out.println("Server started!");
        server.awaitTermination();
    }
}
