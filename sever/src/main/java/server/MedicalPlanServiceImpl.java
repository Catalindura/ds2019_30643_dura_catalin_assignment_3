package server;

import Business.IntakeBusiness;
import Entity.Intake;
import io.grpc.stub.StreamObserver;
import org.baeldung.grpc.IntakeResponse;
import org.baeldung.grpc.MedicalPlanGrpc;
import org.baeldung.grpc.MedicalPlanRequest;
import org.baeldung.grpc.MedicalPlanResponse;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class MedicalPlanServiceImpl extends MedicalPlanGrpc.MedicalPlanImplBase {

    IntakeBusiness intakeBusiness=new IntakeBusiness();

    @Override
    public void getIntakes(
            MedicalPlanRequest request, StreamObserver<MedicalPlanResponse> responseObserver) {
        System.out.println("Request received from client:\n" + request);

        List<Intake> intakes = intakeBusiness.getAllIntakes();
        for (Intake intake:intakes) {
            System.out.println(intake.getTaken()+intake.getPatient().getName());
            if (intake.getTaken().equals("---")) {
                String namePatient = intake.getPatient().getName();
                String startTime = intake.getStartTime();
                String endTime = intake.getEndTime();
                String nameMedication = intake.getMedication().getName();
                String taken = intake.getTaken();
                String id = intake.getIdIntake() + "";
                MedicalPlanResponse plan = MedicalPlanResponse.newBuilder()
                        .setNamePatient(namePatient)
                        .setEndTime(endTime)
                        .setStartTime(startTime)
                        .setNameMedication(nameMedication)
                        .setTaken(taken)
                        .setId(id)
                        .build();
                responseObserver.onNext(plan);
            }
        }
        responseObserver.onCompleted();
    }

    @Override
    public void sendIntake(MedicalPlanResponse medicalPlanResponse,StreamObserver<IntakeResponse> response) {
        System.out.println("Primit de la client"+ medicalPlanResponse.toString());
        Intake intake = intakeBusiness.getIntakeById(Integer.parseInt(medicalPlanResponse.getId()));
        if (medicalPlanResponse.getTaken().equals("taken")) {
            intake.setTaken("taken");
            intakeBusiness.updateIntake(intake);
        }
        else{
            intake.setTaken("not taken");
            intakeBusiness.updateIntake(intake);
        }
        response.onNext(IntakeResponse.newBuilder()
                .setStatus("receptionat").build());
        response.onCompleted();
    }
}
